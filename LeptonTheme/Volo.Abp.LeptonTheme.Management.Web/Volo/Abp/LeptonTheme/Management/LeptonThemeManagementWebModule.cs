﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Volo.Abp.AspNetCore.Mvc.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared;
using Volo.Abp.Authorization;
using Volo.Abp.AutoMapper;
using Volo.Abp.LeptonTheme.Management.Localization;
using Volo.Abp.LeptonTheme.Management.Settings;
using Volo.Abp.Modularity;
using Volo.Abp.SettingManagement.Web.Pages.SettingManagement;
using Volo.Abp.UI.Navigation;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.LeptonTheme.Management
{
    [DependsOn(
		typeof(AbpAutoMapperModule),
		typeof(AbpAuthorizationModule),
		typeof(LeptonThemeManagementHttpApiModule),
		typeof(AbpAspNetCoreMvcUiThemeSharedModule)
	)]
	public class LeptonThemeManagementWebModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			context.Services.PreConfigure<AbpMvcDataAnnotationsLocalizationOptions>(options =>
			{
				options.AddAssemblyResource(typeof(LeptonThemeManagementResource), new Assembly[]
				{
					typeof(LeptonThemeManagementWebModule).Assembly
				});
			});
			base.PreConfigure<IMvcBuilder>(mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(LeptonThemeManagementWebModule).Assembly);
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpNavigationOptions>(options =>
			{
				options.MenuContributors.Add(new LeptonThemeManagementMenuContributor());
			});
			base.Configure<AbpVirtualFileSystemOptions>(options =>
			{
				options.FileSets.AddEmbedded<LeptonThemeManagementWebModule>("Volo.Abp.LeptonTheme.Management");
			});
			context.Services.AddAutoMapperObjectMapper<LeptonThemeManagementWebModule>();
			base.Configure<AbpAutoMapperOptions>(options =>
			{
				options.AddProfile<LeptonThemeManagementWebAutoMapperProfile>(true);
			});
			base.Configure<SettingManagementPageOptions>(options =>
			{
				options.Contributors.Add(new LeptonThemeSettingManagementPageContributor());
			});
			base.Configure<AbpBundlingOptions>(options =>
			{
				options.ScriptBundles.Configure(typeof(IndexModel).FullName, configuration =>
				{
					configuration.AddFiles(new string[]
					{
						"/Pages/LeptonThemeManagement/Components/LeptonThemeSettingGroup/Default.js"
					});
				});
			});
			base.Configure<RazorPagesOptions>(options =>
			{
			});
		}
	}
}
