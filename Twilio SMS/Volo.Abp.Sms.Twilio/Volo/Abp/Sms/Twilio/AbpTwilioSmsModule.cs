﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace Volo.Abp.Sms.Twilio
{
	[DependsOn(
		typeof(AbpSmsModule)
	)]
	public class AbpTwilioSmsModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			IConfiguration configuration = context.Services.GetConfiguration();
			base.Configure<AbpTwilioSmsOptions>(configuration.GetSection("AbpTwilioSms"));
		}
	}
}
