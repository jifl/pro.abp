﻿using System;
using Volo.Abp;
using Volo.Abp.Localization;

namespace Volo.Payment
{
    public class PaymentGatewayConfiguration
	{
		public string Name { get; }

		private ILocalizableString _displayName;

		public ILocalizableString DisplayName
		{
			get
			{
				return this._displayName;
			}
			set
			{
				this._displayName = Check.NotNull<ILocalizableString>(value, nameof(value));
			}
		}

		public Type PaymentGatewayType { get; }

		public int Order { get; set; }

		public PaymentGatewayConfiguration(string name, ILocalizableString displayName, Type paymentGatewayType)
		{
			this.Order = 1000;
			this.Name = Check.NotNullOrWhiteSpace(name, nameof(name));
			this.DisplayName = displayName;
			this.PaymentGatewayType = paymentGatewayType;
		}
	}
}
