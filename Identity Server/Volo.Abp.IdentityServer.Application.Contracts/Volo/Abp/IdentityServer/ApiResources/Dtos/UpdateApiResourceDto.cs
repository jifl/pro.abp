﻿using System;
using Volo.Abp.ObjectExtending;

namespace Volo.Abp.IdentityServer.ApiResources.Dtos
{
	public class UpdateApiResourceDto : ExtensibleObject
	{
		public string DisplayName { get; set; }

		public string Description { get; set; }

		public bool Enabled { get; set; }

		public string[] Claims { get; set; }

		public ApiScopeDto[] Scopes { get; set; }

		public ApiSecretDto[] Secrets { get; set; }
	}
}
