﻿using AutoMapper;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.MainMenu;
using Volo.Abp.AutoMapper;
using Volo.Abp.UI.Navigation;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.ObjectMapping
{
    public class LeptonThemeAutoMapperProfile : Profile
	{
		public LeptonThemeAutoMapperProfile()
		{
			CreateMap<ApplicationMenu, MenuViewModel>().ForMember(p => p.Menu, opt => opt.MapFrom(x => x));
			CreateMap<ApplicationMenuItem, MenuItemViewModel>().ForMember(p => p.MenuItem, opt => opt.MapFrom(x => x)).Ignore(p => p.IsActive);
		}
	}
}
